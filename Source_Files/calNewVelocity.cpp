#include <cmath>
#include <omp.h>
#include <iostream>

#include "Resolution.h"


void calNewVelocity
    (
    // ======================================================== //
    double dt,

    double (*p)[ny][nz],
    double (*u_star)[ny][nz],
    double (*v_star)[ny][nz],
    double (*w_star)[ny][nz],

    double (*u1)[ny][nz],
    double (*v1)[ny][nz],
    double (*w1)[ny][nz],
    
    double (*u2)[ny][nz],
    double (*v2)[ny][nz],
    double (*w2)[ny][nz],

    double (*FX)[ny][nz],
    double (*FY)[ny][nz],
    double (*FZ)[ny][nz],

    double (*ETA)[ny][nz],

    
    double (*iDx),
    double (*Dxs),
    double (*iDy),
    double (*Dys),
    double (*iDz),
    double (*Dzs)
    // ======================================================== //

    )
{
    // ======================================================== //
    double u_solid = 0.0; 
    double v_solid = 0.0; 
    double w_solid = 0.0; 
    

    // ======================================================== //

    


    /**************************************************************/
    /*        Calculation of velocity field at t = dt*n+1         */
    /**************************************************************/

    //In x direction
    for(size_t i = gCells; i < nx-gCells; ++i )
    {
        for(size_t j = gCells; j < ny-gCells; ++j )
        {
            for(size_t k = gCells; k < nz-gCells; ++k)
            {
                u1[i][j][k] = u_star[i][j][k] - dt*( p[i+1][j][k]-p[i][j][k] ) / Dxs[i];
            }
        }    
    }


    //In y direction
    for(size_t i = gCells; i < nx-gCells; ++i )
    {
        for(size_t j = gCells; j < ny-gCells; ++j )
        {
            for(size_t k = gCells; k < nz-gCells; ++k)
            {
                v1[i][j][k] = v_star[i][j][k] - dt*( p[i][j+1][k]-p[i][j][k] ) / Dys[j];
            }
        }    
    }

    //In z direction
    for(size_t i = gCells; i < nx-gCells; ++i )
    {
        for(size_t j = gCells; j < ny-gCells; ++j )
        {
            for(size_t k = gCells; k < nz-gCells; ++k)
            {
                w1[i][j][k] = w_star[i][j][k] - dt*( p[i][j][k+1]-p[i][j][k] ) / Dzs[k];
            }
        }    
    }


    /**************************************************************/
    /*         Calculation of velocity field for DFIB             */
    /**************************************************************/


    for(size_t i = gCells; i < nx-gCells; ++i )
    {
        for(size_t j = gCells; j < ny-gCells; ++j )
        {
            for(size_t k = gCells; k < nz-gCells; ++k)
            {
                u2[i][j][k] = ETA[i][j][k] * u_solid + (1-ETA[i][j][k]) * u1[i][j][k];
                FX[i][j][k] = (u2[i][j][k] - u1[i][j][k]) / dt;
            }
        }    
    }


    for(size_t i = gCells; i < nx-gCells; ++i )
    {
        for(size_t j = gCells; j < ny-gCells; ++j )
        {
            for(size_t k = gCells; k < nz-gCells; ++k)
            {
                v2[i][j][k] = ETA[i][j][k] * v_solid + (1-ETA[i][j][k]) * v1[i][j][k];
                FY[i][j][k] = (v2[i][j][k] - v1[i][j][k]) / dt;
            }
        }    
    }
    
    for(size_t i = gCells; i < nx-gCells; ++i )
    {
        for(size_t j = gCells; j < ny-gCells; ++j )
        {
            for(size_t k = gCells; k < nz-gCells; ++k)
            {
                w2[i][j][k] = ETA[i][j][k] * w_solid + (1-ETA[i][j][k]) * w1[i][j][k];
                FZ[i][j][k] = (w2[i][j][k] - w1[i][j][k]) / dt;
            }
        }    
    }


}

void updating
    (
    // ======================================================== //




    double (*u)[ny][nz],
    double (*v)[ny][nz],
    double (*w)[ny][nz],

    double (*u2)[ny][nz],
    double (*v2)[ny][nz],
    double (*w2)[ny][nz]


    // ======================================================== //

    )
{
    // ======================================================== //
    
    
    // ======================================================== //

    for(size_t i = 0; i < nx; ++i )
    {
        for(size_t j = 0; j < ny; ++j )
        {
            for(size_t k = 0; k < nz; ++k)
            {
       
                
                u[i][j][k] = u2[i][j][k];
                v[i][j][k] = v2[i][j][k];
                w[i][j][k] = w2[i][j][k];

                
                
                
            }
        }    
    }

    
}




























