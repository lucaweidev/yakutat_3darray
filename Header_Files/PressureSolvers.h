#ifndef _PRESSURESOLVERS_INCLUDE_
#define _PRESSURESOLVERS_INCLUDE_



void SuccessiveOverRelaxation
    (
        double zeta, int itmax, double dt, double omega,
        double (*pre)[ny][nz],
        double (*p)[ny][nz],
        double (*u_star)[ny][nz],
        double (*v_star)[ny][nz],
        double (*w_star)[ny][nz],

        double (*iDx),
        double (*Dxs),
        double (*iDy),
        double (*Dys),
        double (*iDz),
        double (*Dzs)
    
    );





#endif