# Makefile 

# Nom du compilateur
CC = g++

# Options de compilation: optimisation, debug etc...
OPT = -O3 -fopenmp -mcmodel=medium 
Linking = 
INC = -I Header_Files/
# -ipo -xCORE-AVX2 -align array32byte -qopenmp -Ofast
# -mcmodel=large
# -heap-arrays 64

EXE = sol0

DIR_OBJ = ./obj
DIR_BIN = ./bin

# Defining the objects (OBJS) variables  P.S. If u want to add cpp file, u can modify here.
OBJS =  \
    main.o \
	gridder.o \
	readingData.o \
	InitialConditions.o \
	BoundaryConditions.o \
	filer.o \
	ConvectionScheme.o \
	PressureSolvers.o \
	calNewVelocity.o \

# Linking object files
exe :  $(OBJS) 
	$(CC) -o $(EXE) \
    $(OBJS) \
    $(OPT) $(Linking)


# echo something
	@echo "   ***************** successful *****************   "                                                                                      
	@echo "    |  Author:  Zi-Hsuan Wei                        "                       
	@echo "    |  Version: 0.2                                 "                   
	@echo "    |  Web:     http://smetana.me.ntust.edu.tw/     "
	@echo "    |  Editor : Hsuan                               "  

# Defining the flags of objects
%.o: Source_Files/%.cpp
	@$(CC) $< $(OPT) $(INC) -c 


# Removing object files
clean :
	@/bin/rm -f *.dat
	@/bin/rm -f *.x
	@/bin/rm -f *.q

cleanall : 
	@/bin/rm -f $(OBJS) $(EXE)  *.mod
	@/bin/rm -f *.dat
	@/bin/rm -f *.x
	@/bin/rm -f *.q
    
config :
	if [ ! -d obj ] ; then mkdir obj ; fi
	if [ ! -d run ] ; then mkdir bin ; fi


