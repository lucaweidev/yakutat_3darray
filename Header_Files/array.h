
#ifndef _ARRAY_
#define _ARRAY_

#include "Resolution.h"
#include "Variables.h"
#include <vector>

using std::vector;
    


    
    //Physical matrix
    // ======================================================== //

    double (*p)[ny][nz] = new double[nx][ny][nz];
    double (*pre)[ny][nz] = new double[nx][ny][nz];

    //vector<vector<vector<double> > > u( nx+4, vector<vector<double> >(ny+4,vector<double> (nz+4)) );
    double (*u)[ny][nz] = new double[nx][ny][nz];
    double (*v)[ny][nz] = new double[nx][ny][nz];
    double (*w)[ny][nz] = new double[nx][ny][nz];

    double (*u1)[ny][nz] = new double[nx][ny][nz];
    double (*v1)[ny][nz] = new double[nx][ny][nz];
    double (*w1)[ny][nz] = new double[nx][ny][nz];

    double (*u2)[ny][nz] = new double[nx][ny][nz];
    double (*v2)[ny][nz] = new double[nx][ny][nz];
    double (*w2)[ny][nz] = new double[nx][ny][nz];

    double (*u_star)[ny][nz] = new double[nx][ny][nz];
    double (*v_star)[ny][nz] = new double[nx][ny][nz];
    double (*w_star)[ny][nz] = new double[nx][ny][nz];
    
    double (*ETA)[ny][nz] = new double[nx][ny][nz];

    double (*FX)[ny][nz] = new double[nx][ny][nz];
    double (*FY)[ny][nz] = new double[nx][ny][nz];
    double (*FZ)[ny][nz] = new double[nx][ny][nz];

    //double (*div1)[ny][nz] = new double[nx][ny][nz];

    
   
    // ======================================================== //

    
    //Grid matrix
    // ======================================================== //

    //Initial grid coordinates for evaluating grid lengths
    double (*X) = new double[nx+1];

    double (*Y) = new double[ny+1];

    double (*Z) = new double[nz+1];



    //Actual grid cooridinates (with adjusted index)
    double (*Xa) = new double[nx-3];

    double (*Ya) = new double[ny-3];

    double (*Za) = new double[nz-3];



    //Grid lengths
    double (*iDx) = new double[nx];

    double (*Dxs) = new double[nx];

    double (*iDy) = new double[ny];

    double (*Dys) = new double[ny];

    double (*iDz) = new double[nz];

    double (*Dzs) = new double[nz];


    //Midpoints of grid coordinate
    double (*Xs) = new double[nx-4];

    double (*Ys) = new double[ny-4];
    
    double (*Zs) = new double[nz-4];


    // ======================================================== //




#endif